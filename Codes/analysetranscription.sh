#! /bin/csh

foreach j (`ls -1 *Omnigenic*.lammpstrj | sort -n`)

    echo $j
    cp $j dumpfile
    ./analyse_transcription < dumpfile > transcription.out
    ./averagetranscriptionrun.2 >> transcriptiondataall.dat

end

./transcriptionstat.4.2
