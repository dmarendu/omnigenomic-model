      program averagetranscriptionsim
      implicit none
      integer*4 ngenemax,ncolbeads,nrunmax
      parameter(ngenemax=50000,ncolbeads=39,nrunmax=1000)
      integer*4 i,iold,j,n,ngene,nrun
      integer*4 imax,icolgene,jcolgene
      integer*4 it_max,it_num,rot_num
      integer*4 dist
      integer*4 key(ncolbeads),mindist(ncolbeads)
      integer*4 colour(ngenemax)
      integer*4 ndata(ngenemax),flag(ngenemax),conn(ngenemax)
      double precision ntranscribed,dmax
      double precision av,av2,std,corr,dx,dy
      double precision maxexpression,eigenvalue,norm
      double precision corrthr
      double precision avtranscribed(ngenemax),av2transcribed(ngenemax)
      double precision stdgene(ngenemax)
      double precision v(ncolbeads,ncolbeads),d(ncolbeads)
      double precision eigenvector(ncolbeads)
      double precision corrmat(ncolbeads,ncolbeads)
      double precision averagegene(ngenemax,nrunmax)
      
      ngene=40000
      corrthr=0.2d0

      do i=1,ngene
         avtranscribed(i)=0.d0
         av2transcribed(i)=0.d0
         ndata(i)=0
!         do j=1,ngene
!            correlation(i,j)=0.d0
!         enddo
         do j=1,nrunmax
            averagegene(i,j)=0.d0
         enddo
         conn(i)=0 ! connections in correlation matrix
         colour(i)=3 ! colour for standard chromatin bead
      enddo

      do i=1,ncolbeads
         do j=1,ncolbeads
            corrmat(i,j)=0.d0
         enddo
      enddo

      open(unit=2,file='key.dat',status='unknown')

      do i=1,ncolbeads
         read(2,*) key(i),colour(key(i))
      enddo

      do i=1,ncolbeads
         mindist(i)=ngene
         if(i.ne.ncolbeads) then
            dist=key(i+1)-key(i)
            if(dist.le.mindist(i)) mindist(i)=dist
         endif
         if(i.ne.1) then
            dist=key(i)-key(i-1)
            if(dist.le.mindist(i)) mindist(i)=dist
         endif
      enddo

      close(2)

      open(unit=2,file='transcriptiondataall.dat',status='unknown')

      nrun=1
      iold=0
 4    read(2,*,end=10) i,ntranscribed
      if(i.lt.iold) nrun=nrun+1
      iold=i
      avtranscribed(i)=avtranscribed(i)+ntranscribed
      av2transcribed(i)=av2transcribed(i)+ntranscribed**2.d0
      ndata(i)=ndata(i)+1
      averagegene(i,nrun)=ntranscribed
      goto 4
 10   close(2)
!      write(6,*) nrun
 
      open(unit=10,file='transcriptionstat.dat',status='unknown')

      n=0
      do i=1,ngene
         if(ndata(i).ne.0) then
            n=n+1
!            av=avtranscribed(i)/dfloat(ndata(i))
!            av2=av2transcribed(i)/dfloat(ndata(i))
            av=avtranscribed(i)/dfloat(nrun)
            av2=av2transcribed(i)/dfloat(nrun)
            std=dsqrt(av2-av**2.d0)/dsqrt(dfloat(nrun)-1.d0)
            write(10,*) i,av,std,mindist(n)
            avtranscribed(i)=av
            stdgene(i)=dsqrt(av2-av**2.d0)
         endif
      enddo

      close(10)

      open(unit=10,file='ranklist.dat',status='unknown')
!     sort expression in a rank list from most to least expressed
         
      do i=1,ngene
         flag(i)=0
      enddo
            
      do j=1,ncolbeads
         maxexpression=-1.d0
         do i=1,ngene
            if(flag(i).eq.0.and.ndata(i).ne.0) then
               if(avtranscribed(i).ge.maxexpression) then
                  maxexpression=avtranscribed(i)
                  imax=i
               endif
            endif
         enddo
         write(10,*) imax,avtranscribed(imax),colour(imax)
         flag(imax)=1
      enddo
         
      close(10)

      open(unit=10,file='correlation.dat',status='unknown')

      icolgene=0
      jcolgene=0

      do i=1,ngene
         if(ndata(i).ne.0) then
            icolgene=icolgene+1
            jcolgene=0
            do j=1,ngene
               if(ndata(j).ne.0) then
                  jcolgene=jcolgene+1
                  corr=0.d0
                  do n=1,nrun
                     dx=averagegene(i,n)-avtranscribed(i)
                     dy=averagegene(j,n)-avtranscribed(j)
                     corr=corr+dx*dy/(stdgene(i)*stdgene(j))
                  enddo
                  corr=corr/dfloat(nrun)
                  write(10,*) i,j,corr,corr*stdgene(i)*stdgene(j)
                  corrmat(icolgene,jcolgene)=corr
                  if(dabs(corr).ge.corrthr) then
                     conn(i)=conn(i)+1
                     conn(j)=conn(j)+1
                  endif
               endif
            enddo
         endif
      enddo

      close(10)

!     diagonalise correlation matrix via jacobi

      it_max=10000

      call jacobi_eigenvalue(ncolbeads,corrmat,it_max,
     1     v,d,it_num,rot_num)

!     write list of eigenvalues, from largest to smallest

      open(unit=10,file='eigenvalues.dat',status='unknown')

      do i=ncolbeads,1,-1
         write(10,*) d(i)
      enddo
      
      close(10)

      open(unit=10,file='eigenvector01.dat',status='unknown')

      do i=1,ncolbeads
         write(10,*) i,v(i,ncolbeads),key(i)
      enddo

      close(10)

      open(unit=10,file='connections.dat',status='unknown')

      do i=1,ngene
         if(conn(i).ne.0) then
            write(10,*) i,conn(i)/2,avtranscribed(i),colour(i)
         endif
      enddo

      stop
      end

      subroutine find_eig(m,v,lambda,n)
      implicit none
      integer*4 nmatmax,nitmax
      parameter(nmatmax=39)
      integer*4 i,j,n
      integer*4 it,nit
      double precision lambda,norm
      double precision v(nmatmax),v_new(nmatmax)
      double precision m(nmatmax,nmatmax)

      nit=100

!      write(6,*) n

      do i=1,n
         v(i)=1.d0
      enddo

      do it=1,nit
         
         norm=0.d0
         do i=1,n
            v_new(i)=0.d0
            do j=1,n
               v_new(i)=v_new(i)+m(i,j)*v(j)
            enddo
            norm=norm+v_new(i)**2.d0
         enddo

         norm=dsqrt(norm)

         do i=1,n
            if(v_new(i).ge.0.1d0) lambda=v_new(i)/v(i)
            v(i)=v_new(i)/norm
         enddo

         norm=0
         do i=1,n
            norm=norm+v(i)**2.d0
         enddo
!         write(6,*) dsqrt(norm),lambda


      enddo

!      do i=1,n
!         write(6,*) v(i)
!      enddo
!      write(6,*) lambda

      return
      end
