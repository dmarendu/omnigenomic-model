      program splitfiles
      implicit none
      integer*4 n,i,j,type,step,nbead,nmax
      parameter(nmax=100000)
      integer*4 nprot,nDNA
      integer*4 activity(nmax)
      integer*4 prottype(nmax),chromtype(nmax)
      double precision threshold,dist2,pbcdist1d
      double precision x(nmax),y(nmax),z(nmax)
      double precision prot(nmax,3),chrom(nmax,3)
      double precision lx,ly,lz
      double precision lx1,lx2,ly1,ly2,lz1,lz2
      character*42 char1,char2
      character*8 str2
      character*12 str1
      character*36 filename
      external pbcdist1d

      nprot=20

      threshold=1.8d0!**2.d0

 3    read(5,*,end=10) char1,char2
      read(5,*) step
      read(5,*,end=10) char1,char2
      read(5,*) nbead
      read(5,*,end=10) char1,char2
      read(5,*) lx1,lx2
      read(5,*) ly1,ly2
      read(5,*) lz1,lz2
      lx=lx2-lx1
      ly=ly2-ly1
      lz=lz2-lz1
      read(5,*,end=10) char1,char2

      nDNA=nbead-nprot
      
!      str1='.protein.dat'
!      write(str2,'(I8.8)') step
!      filename=str2//str1

!      open(unit=10,file=filename,status='unknown')

      do i=1,nbead
            read(5,*) n,type,x(n),y(n),z(n)
            if(n.le.nprot) then
               prot(n,1)=x(n)!*lx-lx/2.d0
               prot(n,2)=y(n)!*ly-ly/2.d0
               prot(n,3)=z(n)!*lz-lz/2.d0
               prottype(n)=type
            else 
               chrom(n-nprot,1)=x(n)!*lx-lx/2.d0
               chrom(n-nprot,2)=y(n)!*ly-ly/2.d0
               chrom(n-nprot,3)=z(n)!*lz-lz/2.d0
               chromtype(n-nprot)=type
            endif
      enddo
 
c     do transcriptional analysis here

      do i=1,nDNA
         activity(i)=0
!     assumes proteins are before chromatin beads
         if(chromtype(i).eq.4) then   ! type 4 = high-affinity sticky beads
            do j=1,nprot
               dist2=pbcdist1d(prot(j,1),chrom(i,1),lx)**2.d0+
     1              pbcdist1d(prot(j,2),chrom(i,2),ly)**2.d0+
     1              pbcdist1d(prot(j,3),chrom(i,3),lz)**2.d0
               if(dist2.le.threshold**2.d0.and.
     1              chromtype(i)-prottype(j).eq.3) then ! type 1 = TF ON
                  activity(i)=prottype(j) 
               endif
            enddo
            write(6,*) step,i,activity(i),chromtype(i) 
         endif
      enddo

      goto 3

 10   continue

      do i=1,nprot
         write(8,*) i,prottype(i),prot(i,1),prot(i,2),prot(i,3)
      enddo
      do i=1,nDNA
         write(8,*) i,chromtype(i),chrom(i,1),chrom(i,2),chrom(i,3)
      enddo

      stop
      end

      function pbcdist1d(x,y,l)
      implicit none
      double precision x,y,l
      double precision dist,pbcdist1d
      dist=dabs(x-y)
      if(dist.ge.l/2.d0) dist=l-dist
      pbcdist1d=dist
      return
      end
