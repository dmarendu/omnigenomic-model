!     this works for randomly chosen beads, not equispaced
      program averagetranscription
      implicit none
      integer*4 ngenemax,ngene
      parameter(ngenemax=100000)
      integer*4 step,i,index,on,type
      integer*4 ndata(ngenemax)
      double precision transcription(ngenemax)

      ngene=50000

      do i=1,ngenemax
         transcription(i)=0
         ndata(i)=0
      enddo
      
      open(unit=3,file='transcription.out',status='unknown')

 3    read(3,*,end=10) step,i,on,type
      if(on.ne.0) then
         index=i
         ndata(index)=ndata(index)+1
         transcription(index)=transcription(index)+1.d0
      endif
      goto 3
 10   close(3)

      do i=1,ngene
         if(ndata(i).ne.0) then
            write(6,*) i,transcription(i)
         endif
      enddo

      stop
      end
